/***********************************************************************************
Copyright (c) 2017, Diego Pardo. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/


/*! \file pointFootLeg_squad.cpp
 *  \author depardo
 */

#include <memory>
#include <iostream>

// Base::Optimization
#include <optimization/constraints/ConstraintsBase.hpp>
#include <optimization/costs/CostFunctionBase.hpp>
#include <optimization/constraints/InterPhaseBaseConstraint.hpp>
#include <optimization/constraints/GuardBase.hpp>

// Base::Dynamics
#include <dynamical_systems/base/LeggedRobotDynamics.hpp>
#include <dynamical_systems/base/DerivativesNumDiffBase.hpp>

//HyQ Headers
#include <dynamical_systems/systems/pointFootLeg/PFLKinematics.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLDimensions.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLProjectedDynamics.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLDerivatives.hpp>
#include <dynamical_systems/systems/pointFootLeg/PFLState.hpp>

// DTO Package
#include <direct_trajectory_optimization_problem.hpp>
#include <optimization/costs/ZeroCostBolza.hpp>


// LRT Package
#include <legged_robot_task_dt/constraints/lrt_singleConstraintBase.hpp>

// HyQ DT Tasks
#include <legged_robot_task_dt/constraints/lrt_setOfConstraints.hpp>
#include <legged_robot_task_dt/constraints/lrt_frictionConeConstraint.hpp>
#include <legged_robot_task_dt/constraints/lrt_surfacePenetrationConstraint.hpp>
#include <legged_robot_task_dt/constraints/lrt_lastPhaseConstraint.hpp>

// Hybrid Control constraints
#include <legged_robot_task_dt/constraints/lrt_contact_guard.hpp>
#include <legged_robot_task_dt/constraints/lrt_liftLegConstraint.hpp>
#include <legged_robot_task_dt/constraints/lrt_impactDynamicsInterPhaseImpulseCone.hpp>

// Interpolate Results
#include <legged_robot_task_dt/tools/DTTrajectorySpliner.hpp>

// Ros visual
#include <ds_visual_base/RobotConfigVis.hpp>

// Analysis tool
#include <dt_examples/DTSolutionAnalysis.hpp>
#include <dynamical_systems/tools/HybridLRTrajectory.hpp>


using namespace DirectTrajectoryOptimization;
using pfld = robotDimensions;

void SetBounds(pfld::GeneralizedCoordinates_t & q_min, pfld::GeneralizedCoordinates_t & q_max,
               pfld::GeneralizedCoordinates_t & qd_min, pfld::GeneralizedCoordinates_t & qd_max);

int main(int argc, char* argv[]) {


	// initial state
  PFLKinematics pflkin;
  pfld::GeneralizedCoordinates_t q_initial,q_zero;
  pfld::state_vector_t initial_state;

  double grnd = pflkin.getDefaultGroundDistance();
  double ground_z = std::floor(grnd * 100000) / 100000;
  pflkin.GetCanonicalPoseState(initial_state);
  pflkin.GeneralizedCoordinatesFromStateVector(initial_state,q_initial,q_zero);

	// HyQ Step
	int phase_1_code = 1; //p;
	int phase_2_code = 0; //f;
	int phase_3_code = 1; //p;
	int phase_4_code = 1; //p;

	enum {
	  number_of_phases = 4
	};

	int contact_sequence[number_of_phases] =
	    { phase_1_code,
	      phase_2_code,
	      phase_3_code,
	      phase_4_code};

	pfld::ContactConfiguration_t feet_configuration_1 = pfld::get_all_contact_configurations()[phase_1_code];
	pfld::ContactConfiguration_t feet_configuration_2 = pfld::get_all_contact_configurations()[phase_2_code];
	pfld::ContactConfiguration_t feet_configuration_3 = pfld::get_all_contact_configurations()[phase_3_code];
  pfld::ContactConfiguration_t feet_configuration_4 = pfld::get_all_contact_configurations()[phase_4_code];

	std::shared_ptr<PFLProjectedDynamics> _hyqpdynamics_1(new PFLProjectedDynamics(feet_configuration_1));
	std::shared_ptr<PFLProjectedDynamics> _hyqpdynamics_2(new PFLProjectedDynamics(feet_configuration_2));
	std::shared_ptr<PFLProjectedDynamics> _hyqpdynamics_3(new PFLProjectedDynamics(feet_configuration_3));
  std::shared_ptr<PFLProjectedDynamics> _hyqpdynamics_4(new PFLProjectedDynamics(feet_configuration_4));

	std::shared_ptr<DynamicsBase<pfld> > dynamicsPhase1(_hyqpdynamics_1);
	std::shared_ptr<DynamicsBase<pfld> > dynamicsPhase2(_hyqpdynamics_2);
	std::shared_ptr<DynamicsBase<pfld> > dynamicsPhase3(_hyqpdynamics_3);
  std::shared_ptr<DynamicsBase<pfld> > dynamicsPhase4(_hyqpdynamics_4);

	std::shared_ptr<DerivativesBaseDS<pfld> > derivativesPhase1(new PFLDerivatives(feet_configuration_1));
	std::shared_ptr<DerivativesBaseDS<pfld> > derivativesPhase2(new PFLDerivatives(feet_configuration_2));
	std::shared_ptr<DerivativesBaseDS<pfld> > derivativesPhase3(new PFLDerivatives(feet_configuration_3));
  std::shared_ptr<DerivativesBaseDS<pfld> > derivativesPhase4(new PFLDerivatives(feet_configuration_4));

	std::shared_ptr<BaseClass::CostFunctionBase<pfld> > costFunction(new Costs::ZeroCostBolza<pfld>());

	std::shared_ptr<lrt::LRTSetOfConstraints<pfld>> hyqsof_1(new lrt::LRTSetOfConstraints<pfld>(std::shared_ptr<PFLProjectedDynamics>(new PFLProjectedDynamics(feet_configuration_1))));
	std::shared_ptr<lrt::LRTSetOfConstraints<pfld>> hyqsof_2(new lrt::LRTSetOfConstraints<pfld>(std::shared_ptr<PFLProjectedDynamics>(new PFLProjectedDynamics(feet_configuration_2))));
	std::shared_ptr<lrt::LRTSetOfConstraints<pfld>> hyqsof_3(new lrt::LRTSetOfConstraints<pfld>(std::shared_ptr<PFLProjectedDynamics>(new PFLProjectedDynamics(feet_configuration_3))));
	std::shared_ptr<lrt::LRTSetOfConstraints<pfld>> hyqsof_4(new lrt::LRTSetOfConstraints<pfld>(std::shared_ptr<PFLProjectedDynamics>(new PFLProjectedDynamics(feet_configuration_4))));

	std::shared_ptr<lrt::LRTSingleConstraintBase<pfld>> cone_1(new lrt::LRTFrictionConeConstraint<pfld>(1.0));
	std::shared_ptr<lrt::LRTSingleConstraintBase<pfld>> cone_2(new lrt::LRTFrictionConeConstraint<pfld>(1.0));
	std::shared_ptr<lrt::LRTSingleConstraintBase<pfld>> cone_3(new lrt::LRTFrictionConeConstraint<pfld>(1.0));
  std::shared_ptr<lrt::LRTSingleConstraintBase<pfld>> cone_4(new lrt::LRTFrictionConeConstraint<pfld>(1.0));

	double contact_surface_tolerance = 5.0e-4;
	std::shared_ptr<lrt::LRTSingleConstraintBase<pfld> > penetration_constraint_1(new lrt::LRTSurfacePenetrationConstraint<pfld>(contact_surface_tolerance,ground_z));
	std::shared_ptr<lrt::LRTSingleConstraintBase<pfld> > penetration_constraint_2(new lrt::LRTSurfacePenetrationConstraint<pfld>(contact_surface_tolerance,ground_z));
	std::shared_ptr<lrt::LRTSingleConstraintBase<pfld> > penetration_constraint_3(new lrt::LRTSurfacePenetrationConstraint<pfld>(contact_surface_tolerance,ground_z));
  std::shared_ptr<lrt::LRTSingleConstraintBase<pfld> > penetration_constraint_4(new lrt::LRTSurfacePenetrationConstraint<pfld>(contact_surface_tolerance,ground_z));

  // Finish standing up in nominal position
	Eigen::VectorXi index(robotDimensions::kTotalDof);
	Eigen::VectorXd max_values(index.size());
	Eigen::VectorXd min_values(index.size());
	index << 0 ,1,2,3,4,5,6,7,8,9;

	double tolerance = 0.01; // try 0.1

	max_values = q_initial.array() + tolerance;
	min_values = q_initial.array() - tolerance;

	size_t x_coordinate = 3;

	min_values(x_coordinate) = -1e20;
	max_values(x_coordinate) = -0.20;

	pfld::state_vector_t final_state;

	std::shared_ptr<lrt::LRTSingleConstraintBase<pfld> > lastphase_const(new lrt::LRTLastPhaseConstraint<pfld>(index,max_values,min_values));

  hyqsof_1->addConstraint(penetration_constraint_1);
	hyqsof_1->addConstraint(cone_1);

	hyqsof_2->addConstraint(penetration_constraint_2);

	hyqsof_3->addConstraint(cone_3);
	hyqsof_3->addConstraint(penetration_constraint_3);

  hyqsof_4->addConstraint(cone_4);
  hyqsof_4->addConstraint(penetration_constraint_4);
  hyqsof_4->addConstraint(lastphase_const);

	hyqsof_1->initialize();
	hyqsof_2->initialize();
	hyqsof_3->initialize();
  hyqsof_4->initialize();

	std::shared_ptr<BaseClass::ConstraintsBase<pfld> > constraints_1(hyqsof_1);
	std::shared_ptr<BaseClass::ConstraintsBase<pfld> > constraints_2(hyqsof_2);
	std::shared_ptr<BaseClass::ConstraintsBase<pfld> > constraints_3(hyqsof_3);
  std::shared_ptr<BaseClass::ConstraintsBase<pfld> > constraints_4(hyqsof_4);

  std::vector<int> legs_to_lift_1 = {0};
  double max_leg_force = 500;
  double mu = 0.7, maxZ = 500;
  std::shared_ptr<BaseClass::InterPhaseBase<pfld> > interPhase_1_2(new lrt::LRTLiftLegConstraint<pfld>(std::shared_ptr<LeggedRobotDynamics<pfld>>(new PFLProjectedDynamics(feet_configuration_1)),feet_configuration_1,legs_to_lift_1,max_leg_force));
  std::shared_ptr<BaseClass::InterPhaseBase<pfld> > interPhase_2_3(new lrt::LRTImpactDynamicsInterPhaseImpulseCone<pfld>(std::shared_ptr<LeggedRobotDynamics<pfld>>(new PFLProjectedDynamics(feet_configuration_2)),
                                                                                                                         std::shared_ptr<LeggedRobotDynamics<pfld>>(new PFLProjectedDynamics(feet_configuration_3)),
                                                                                                                         mu,maxZ));
  std::shared_ptr<BaseClass::InterPhaseBase<pfld> > interPhase_3_4(new BaseClass::InterPhaseBase<pfld>(true));

	interPhase_1_2->initialize();
	interPhase_2_3->initialize();
  interPhase_3_4->initialize();

	// at the points where the two dynamics touch, we require a guard function, (a function of the state!)
	// guard function : the condition that should be true at the point of switching dynamics
	// in HyQProjected dynamics this condition is FeetPos = ground;
	pfld::ContactConfiguration_t feet_3(true);

  double guard_tolerance = 5.0e-4;
	std::shared_ptr<BaseClass::GuardBase<pfld> > guard_1_2(new BaseClass::GuardBase<pfld>(true));
	std::shared_ptr<BaseClass::GuardBase<pfld> > guard_2_3(new lrt::LRTContactGuard<pfld>(std::shared_ptr<LeggedRobotDynamics<pfld>>(new PFLProjectedDynamics(feet_configuration_3)),feet_configuration_3,guard_tolerance,ground_z));
  std::shared_ptr<BaseClass::GuardBase<pfld> > guard_3_4(new BaseClass::GuardBase<pfld>(true));

	guard_1_2->initialize();
	guard_2_3->initialize();
	guard_3_4->initialize();

	// store phases in vector
	std::vector<std::shared_ptr<DynamicsBase<pfld> > > dynamicsVector;
	std::vector<std::shared_ptr<DerivativesBaseDS<pfld> > > derivativesVector;
	std::vector<std::shared_ptr<BaseClass::ConstraintsBase<pfld> > > constraintsVector;
	std::vector<std::shared_ptr<BaseClass::InterPhaseBase<pfld> > > interPhaseVector;
	std::vector<std::shared_ptr<BaseClass::GuardBase<pfld> > > guardsVector;

	dynamicsVector.push_back(dynamicsPhase1);
	dynamicsVector.push_back(dynamicsPhase2);
	dynamicsVector.push_back(dynamicsPhase3);
  dynamicsVector.push_back(dynamicsPhase4);

	derivativesVector.push_back(derivativesPhase1);
	derivativesVector.push_back(derivativesPhase2);
	derivativesVector.push_back(derivativesPhase3);
  derivativesVector.push_back(derivativesPhase4);

	constraintsVector.push_back(constraints_1);
	constraintsVector.push_back(constraints_2);
	constraintsVector.push_back(constraints_3);
  constraintsVector.push_back(constraints_4);

	interPhaseVector.push_back(interPhase_1_2);
	interPhaseVector.push_back(interPhase_2_3);
  interPhaseVector.push_back(interPhase_3_4);

	guardsVector.push_back(guard_1_2);
	guardsVector.push_back(guard_2_3);
	guardsVector.push_back(guard_3_4);

	// Getting parameters from the file
	double Tmin = 0.01;
	double Tmax = 1e20;
	Eigen::Vector2d duration;
	duration <<  Tmin, Tmax;
	int number_of_nodes = 20;
	int dircol_method = 1;
	bool method_verbose = false;
	bool solver_verbose = false;
	bool even_time_increments = false;
	bool feasible_only = false;
	bool automatic_differentiation = false;
	DirectTrajectoryOptimization::Solvers::solver_t nlp_solver = static_cast<DirectTrajectoryOptimization::Solvers::Solver>(0);
	DirectTrajectoryOptimization::Methods::method_t method_type= static_cast<DirectTrajectoryOptimization::Methods::Method>(0);

	std::unique_ptr<DirectTrajectoryOptimization::DirectTrajectoryOptimizationProblem<pfld> > multipleDynamicsDTOP =
			DirectTrajectoryOptimization::DirectTrajectoryOptimizationProblem<pfld>::CreateWithMultipleDynamics(
					dynamicsVector,
					derivativesVector,
					costFunction,
					constraintsVector,
					duration,
					number_of_nodes,
					nlp_solver,
					method_type,
					solver_verbose,
					method_verbose,
					feasible_only
					);

	if(!multipleDynamicsDTOP) {
		std::cout << "Please install the corresponding SOLVER." << std::endl;
		exit(EXIT_FAILURE);
	}

	// State bounds
	  pfld::GeneralizedCoordinates_t q_min,q_max,qd_min,qd_max;
		pfld::state_vector_t x_min,x_max;

		SetBounds(q_min,q_max,qd_min,qd_max);

		pflkin.StateVectorFromGeneralizedCoordinates(q_min,qd_min,x_min);
		pflkin.StateVectorFromGeneralizedCoordinates(q_max,qd_max,x_max);

	// Initial (and final) Control  u(t=0)
		pfld::control_vector_t u_inverse_dynamics;
		PFLProjectedDynamics local(feet_configuration_1);
		local.updateState(initial_state);
		local.InverseDynamicsControl(pfld::GeneralizedCoordinates_t::Zero(),u_inverse_dynamics);
		std::cout << "This is the inverse dynamics control: " << std::endl;
		std::cout << u_inverse_dynamics.transpose() << std::endl;

	// Control bounds
		pfld::control_vector_t u_min, u_max;
		u_min.setConstant(-200);
		u_max.setConstant(200);

	// Setting values to DT
		multipleDynamicsDTOP->SetInitialState(initial_state);
		multipleDynamicsDTOP->SetInitialControl(u_inverse_dynamics);

	// Method parameters
		multipleDynamicsDTOP->SetDircolMethod(dircol_method);
		multipleDynamicsDTOP->SetEvenTimeIncrements(even_time_increments);
		multipleDynamicsDTOP->_method->SetControlBounds(u_min,u_max);
		multipleDynamicsDTOP->_method->SetStateBounds(x_min,x_max);

	// Solver Parameters
    int derivatives_verification = -1;

		bool use_print_file = false;

		multipleDynamicsDTOP->_solver->SetVerifyLevel(derivatives_verification);
		multipleDynamicsDTOP->_solver->UsePrintFile(use_print_file);

	// we explicitly set node split percentages (if not specified, equal split is used)
		Eigen::VectorXi nodeSplit;
		nodeSplit.resize(dynamicsVector.size());
		nodeSplit(0) = 5;	//
		nodeSplit(1) = 5;	//
		nodeSplit(2) = 5; // standing
		nodeSplit(3) = 5;	// standing

	// set up details of problem
		multipleDynamicsDTOP->_method->SetInterPhaseFunctions(guardsVector,interPhaseVector);
		//multipleDynamicsDTOP->SetPercentageNodesPerDynamics(nodeSplit);
		multipleDynamicsDTOP->_method->SetNodesPerPhase(nodeSplit);

	// preparing problem name and initializing the solver
		std::string solver_output_file =  "solver_file.dto";
		multipleDynamicsDTOP->InitializeSolver("pfl", solver_output_file , "",automatic_differentiation);

	// Warm Initialization
		bool solver_flag = false;

		pfld::control_vector_array_t u_initial(number_of_nodes,u_inverse_dynamics);
		pfld::state_vector_array_t y_initial(number_of_nodes,initial_state);
		Eigen::VectorXd h_initial(number_of_nodes-number_of_phases);
		h_initial.setConstant(0.05);

		multipleDynamicsDTOP->_solver->InitializeDecisionVariablesFromTrajectory(y_initial,u_initial,h_initial);

	// Solve!
		multipleDynamicsDTOP->Solve(solver_flag);

	//Getting the solution
		pfld::state_vector_array_t y_trajectory;
		pfld::control_vector_array_t u_trajectory;
		Eigen::VectorXd  h_trajectory;
		std::vector<int> phaseId;

		multipleDynamicsDTOP->GetDTOSolution(y_trajectory, u_trajectory, h_trajectory, phaseId);

	// Show Solution
		int trajectory_size = y_trajectory.size();
		std::cout << "h_index : " << std::endl
				<< h_trajectory.transpose() << std::endl;

		std::vector<int> hyqPhaseCode;
		for(int i = 0 ; i < phaseId.size() ; i++){
				hyqPhaseCode.push_back(contact_sequence[phaseId[i]]);
		}

		Eigen::VectorXd F_local;
		multipleDynamicsDTOP->GetFVector(F_local);

		std::cout << "F: " << F_local(0) << std::endl;
		std::cout << std::endl << "... end of App."  << std::endl;

//		Interpolate result :
	  std::shared_ptr<DTTrajectorySpliner<pfld>> spliner(new DTTrajectorySpliner<pfld>(_hyqpdynamics_1));

		std::shared_ptr<LeggedRobotTrajectory<pfld>> lrt(new LeggedRobotTrajectory<pfld>(_hyqpdynamics_2));
//
	      //Temporal, this should be in LRT
        Eigen::VectorXd time_t(y_trajectory.size());
        time_t(0) = 0.0;

        int node = 0;
        for(int k = 1 ; k < y_trajectory.size() ; ++k) {
            if(phaseId[k] == phaseId[k-1]) {
                time_t(k) = time_t(k-1) + h_trajectory(node);
                node++;
            } else {
                time_t(k) = time_t(k-1);
            }
        }
    lrt->updateLRTrajectory(y_trajectory,u_trajectory,time_t,phaseId,hyqPhaseCode);
	  bool hybrid_ok = spliner->setHybridTrajectory(y_trajectory,u_trajectory,time_t,phaseId,hyqPhaseCode);
//
	  spliner->interpolateFromHybird();
//
	  pfld::state_vector_array_t x_;
	  pfld::control_vector_array_t u_;
	  Eigen::VectorXd t_;
	  std::vector<int> p_;
	  std::vector<int> c_;
	  bool is_ready = spliner->getInterpolatedTrajectory(x_,u_,t_,p_,c_);

		ros::init(argc,argv,"playbackPFLDT");
    RobotConfigVis<pfld> pfl_vis("pointfootleg");
    for(auto x: y_trajectory) {
         pfl_vis.visualize_pose(x);
         std::getchar();
    }

    DTSolutionAnalysis<pfld> sol_test(lrt,F_local);
    sol_test.analyse();
}


void SetBounds(pfld::GeneralizedCoordinates_t & q_min, pfld::GeneralizedCoordinates_t & q_max,
		pfld::GeneralizedCoordinates_t & qd_min, pfld::GeneralizedCoordinates_t & qd_max) {


		/* joints positions */
			q_min[6]  = -0.5;
			q_max[6]  =  0.5;


			q_min[7]  = -0.8;
			q_max[7]  =  1.2;

			q_min[8]  = -1.9;
			q_max[8]  = -0.3;

		/* joints velocities*/
		for(int i = 0 ; i < pfld::kTotalJoints ; ++i) {
			qd_min[i+pfld::kBaseDof] = -10;//-1.2; // -57
			qd_max[i+pfld::kBaseDof] = 10;//1.2; // 57
		}

		/* base limits */
			q_min[0] =  -M_PI/2;//alpha
			q_min[1] =  -M_PI/2;//beta
			q_min[2] =  -M_PI/2;//gamma
			q_min[3] =  -10;    //x
			q_min[4] =  -10;    //y
			q_min[5] =  -5;     //z

			q_max[0] = M_PI/2;  //alpha
			q_max[1] = M_PI/2;  //beta
			q_max[2] = M_PI/2;  //gamma
			q_max[3] =  10;  //x
			q_max[4] = 10;   //y
			q_max[5] = 10;   //z

			qd_min[0] = -10; //alpha
			qd_min[1] = -10; //beta
			qd_min[2] = -10; //gamma
			qd_min[3] = -10; //x
			qd_min[4] = -10; //y
			qd_min[5] = -10; //z

			qd_max[0] = 10;  //alpha : 30
			qd_max[1] = 10;  //beta  : 45
			qd_max[2] = 10;  //gamma : 10
			qd_max[3] = 10;  //x
			qd_max[4] = 10;  //y
			qd_max[5] = 10;  //z
}

